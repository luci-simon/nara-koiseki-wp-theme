<!DOCTYPE html>
<html>
<head>
  <title>Nara-Koiseki auditores independentes</title>
  
  <link href="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet">

  <style type="text/css">
    body {
      margin: 0;

      font-family: 'Roboto', sans-serif;
      font-weight: 300;
      color: #231F20;
    }

    main {
      display: flex;
      margin: auto auto;

      padding: 30px;
      box-sizing: border-box;

      flex-direction: column;
      justify-content: center;
      align-items: flex-start;
    }

    h1 {
      font-weight: 300;
    }

    address {
      margin-top: 40px;
      font-style: normal;
    }

    address a {
      text-decoration: none;
      color: inherit;
    }

    @media (min-width: 801px) {
      main {
        width: 100%;
        height: 100vh;
        max-width: 860px;
      }

      h1 {
        font-size: 30px;
      }

      #logo-main {
        width: 280px;
      }

      address {
        font-size: 18px;
      }

      #logo-illustration {
        position: fixed;
        bottom: 100px;
        right: 100px;
      }
    }

    @media (min-width: 401px) and (max-width: 800px) {
      main {
        width: 100%;
        height: 100vh;
        max-width: 600px;
      }

      h1 {
        font-size: 20px;
      }

      #logo-main {
        width: 220px;
      }

      address {
        font-size: 14px;
      }

      #logo-illustration {
        margin-top: 40px;
        align-self: flex-end;
        margin-bottom: 40px;
      }
    }

    @media (max-width: 400px) {
      main {
        width: 100%;
      }

      h1 {
        font-size: 20px;
      }

      #logo-main {
        width: 220px;
      }

      address {
        font-size: 14px;
      }

      #logo-illustration {
        margin-top: 40px;
        align-self: center;
        margin-bottom: 40px;
      }
    }

  </style>

</head>

<body>
  <main>
    <h1>Em breve</h1>
    <img id="logo-main" src="<?php echo get_template_directory_uri(); ?>/resources/img/logo-full.svg">

    <address>
      R. Conceição Veloso, 205 - Vila Mariana<br>
      CEP 04110-120 - São Paulo/SP<br>

      <br>

      <b>t</b> <a href="tel:+55 11 5572 4156">+55 11 5572 4156</a><br>
      <b>t</b> <a href="tel:+55 11 5572 4638">+55 11 5572 4638</a>
    </address>

    <img id="logo-illustration" src="<?php echo get_template_directory_uri(); ?>/resources/img/logo.svg">
  </main>
</body>

</html>